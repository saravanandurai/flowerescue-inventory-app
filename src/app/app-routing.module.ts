import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralGuard } from './guards/general/general.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule',
    canActivate: [GeneralGuard],
  },
  { 
    path: 'flower-types-list', 
    loadChildren: './pages/flower-types-list/flower-types-list.module#FlowerTypesListPageModule', 
    canActivate: [GeneralGuard],
  },
  { 
    path: 'flowers-in/:id', 
    loadChildren: './pages/flowers-in/flowers-in.module#FlowersInPageModule',    
    canActivate: [GeneralGuard], 
  },  
  { 
    path: 'flowers-in-multi/:id', 
    loadChildren: './pages/flowers-in-multi/flowers-in-multi.module#FlowersInMultiPageModule',    
    canActivate: [GeneralGuard], 
  },
  { 
    path: 'flowers-waste/:id', 
    loadChildren: './pages/flowers-waste/flowers-waste.module#FlowersWastePageModule', 
    canActivate: [GeneralGuard], 
  },
  { 
    path: 'flower-report', 
    loadChildren: './pages/flower-report/flower-report.module#FlowerReportPageModule', 
    canActivate: [GeneralGuard], 
  },
  { 
    path: 'flower-type-master/:id', 
    loadChildren: './pages/flower-type-master/flower-type-master.module#FlowerTypeMasterPageModule', 
    canActivate: [GeneralGuard], 
  },
  { 
    path: 'flowers-in-list', 
    loadChildren: './pages/flowers-in-list/flowers-in-list.module#FlowersInListPageModule', 
    canActivate: [GeneralGuard], 
  },
  { 
    path: 'flowers-waste-list', 
    loadChildren: './pages/flowers-waste-list/flowers-waste-list.module#FlowersWasteListPageModule', 
    canActivate: [GeneralGuard], 
  },
  { 
    path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { 
    path: 'login', 
    loadChildren: './pages/login/login.module#LoginPageModule' },   { path: 'flowers-in-multi', loadChildren: './pages/flowers-in-multi/flowers-in-multi.module#FlowersInMultiPageModule' },
  { path: 'flowers-in-multi', loadChildren: './pages/flowers-in-multi/flowers-in-multi.module#FlowersInMultiPageModule' },
  { 
    path: 'flowers-waste-multi/:id', 
    loadChildren: './pages/flowers-waste-multi/flowers-waste-multi.module#FlowersWasteMultiPageModule' 
  },

 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
