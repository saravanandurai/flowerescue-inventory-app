import { TestBed } from '@angular/core/testing';

import { FlowersInService } from './flowers-in.service';

describe('FlowersInService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlowersInService = TestBed.get(FlowersInService);
    expect(service).toBeTruthy();
  });
});
