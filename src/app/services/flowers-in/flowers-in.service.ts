import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

import { Storage } from '@ionic/storage';

import { STORAGE_KEYS, SAVE_SUCCESS, DELETE_SUCCESS, UPDATE_SUCCESS, ACTION_FAILED } from '../../constants';
import { idGenerator } from '../helpers';

import FlowersIn from '../../interfaces/flowersIn';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';
import { UserService } from '../user/user.service';


@Injectable({
  providedIn: 'root'
})
export class FlowersInService {
  fICollRef: AngularFirestoreCollection; 
  constructor(
    private storage: Storage,
    private db: AngularFirestore, 
    private toastController: ToastController,
    private afAuth: AngularFireAuth,
    private userService: UserService
  ) {
    this.fICollRef = this.db.collection(STORAGE_KEYS.FLOWERS_IN);
   }

  async getFlowersIn(id: string) {
    try {      
      const docRf = await this.fICollRef.doc(id);
      const doc = await docRf.get().pipe(first()).toPromise();      
      if(doc.exists) {
        const data = doc.data();
        delete data.companyId;
        delete data.createdByUid;
        delete data.createdDate;
        delete data.modifiedByUid;
        delete data.modifiedDate;
        return {status: 'ok', message: 'success', payload: data};
      }
      throw {status: 'error', message:'Document does not exists.', payload: null};      
    } catch(e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }   
  }

  async getFlowersInList() {
    try {      
      const profile = await this.userService.getUser();
      const list = await this.fICollRef.ref.where('companyId', '==', profile.payload.companyId).get();       
      if(list.empty) {
        throw {status: 'error', message: 'No Flowers In Found', payload: null}
      }
      let arr = [];
      list.forEach(doc => {      
        arr.push(Object.assign({id: doc.id}, doc.data()))
      })

      return {status: 'ok', message: 'Success', payload: arr}

    } catch(e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }   
  }

  async createFlowersIn(flowersIn: FlowersIn) { 
    try{
      const authData = await this.afAuth.user.pipe(first()).toPromise();
      flowersIn.createdByUid = authData.uid; 
      flowersIn.createdDate = new Date().toISOString();
      const profile = await this.userService.getUser();
      flowersIn.companyId = profile.payload.companyId;  
      const result  = await this.fICollRef.add(flowersIn)
      return {status: 'ok', message: 'success', payload: result.id}
    } catch (e) {
      console.log('Error', e);
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }      
  }

  async updateFlowersIn(id: string, flowersIn: FlowersIn) {
    if(id === '0') {
      this.presentToast('Id cannot be 0 in update', 'danger'); 
      throw {status: 'error', message: 'Id cannot be 0 in update', payload: null};
    }

    try {
      const docRf = await this.fICollRef.doc(id);
      const user = await this.afAuth.user.pipe(first()).toPromise();
      flowersIn.modifiedByUid = user.uid;
      flowersIn.modifiedDate = new Date().toISOString();
      const result = await docRf.update(flowersIn);
      return UPDATE_SUCCESS    
    } catch (e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }   
  }

  async deleteFlowersIn(id: string) {
    if(id === '0') {
      this.presentToast('Id cannot be 0 in delete', 'danger');
      throw {status: 'error', message: 'Id cannot be 0 in delete', payload: null};     
    }
    try {
      const docRf = await this.fICollRef.doc(id);
      const status = await docRf.delete();
      return {status:'ok', message: 'Deleted Successfully', payload: null};
    } catch (e) {
      this.presentToast(e.message, 'danger'); 
      throw {status: 'error', message: e.message, payload: null}
    } 
  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }
}
