import { TestBed } from '@angular/core/testing';

import { FlowerTypesService } from './flower-types.service';

describe('FlowerTypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlowerTypesService = TestBed.get(FlowerTypesService);
    expect(service).toBeTruthy();
  });
});
