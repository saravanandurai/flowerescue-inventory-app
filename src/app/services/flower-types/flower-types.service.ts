import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

import FlowerType from '../../interfaces/flowertype';

import { STORAGE_KEYS, SAVE_SUCCESS, DELETE_SUCCESS, UPDATE_SUCCESS, ACTION_FAILED, GET_OPTIONS } from '../../constants'
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'
import { first } from 'rxjs/operators';
import { UserService } from '../user/user.service';


@Injectable({
  providedIn: 'root'
})
export class FlowerTypesService {
  typeCollRf: AngularFirestoreCollection;  
  
  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private toastController: ToastController,
    private userService: UserService,
  ) {
    this.typeCollRf = this.db.collection(STORAGE_KEYS.FLOWER_TYPES);
  }


  async createFlowerType(flowerType: FlowerType) {
    try{
      const user = await this.afAuth.authState.pipe(first()).toPromise();
      const profile = await this.userService.getUser();      
      flowerType.createdByUid = user.uid;
      flowerType.createdDate = new Date().toISOString();
      flowerType.companyId = profile.payload.companyId;
      const result = await this.typeCollRf.add(flowerType);         
      return {status: 'ok', message: 'Success', payload: result.id};      
    } catch (e) {
      console.log('Error', e);
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null};
    }      
  }

  async updateFlowerType(flowerType: FlowerType) {   
    if(flowerType.id === '0'){
      return {status:'error', message: 'ID cannot be 0 for a update', payload: null}
    }

    try {
      const user = await this.afAuth.authState.pipe(first()).toPromise();
      flowerType.modifiedByUid = user.uid;
      flowerType.modifiedDate = new Date().toISOString();
      const id = flowerType.id; 
      delete flowerType.id;
      const result = await this.typeCollRf.doc(id).update(flowerType);
      return UPDATE_SUCCESS;
    } catch (e) {
      console.log('ERR', e);
      this.presentToast(e.message, 'danger');
      throw {status:'error', message: e.message, payload: null};     
    }    
  }

  async getFlowerType(id: string) {
    try {
      const docRf = await this.typeCollRf.doc(id);
      const doc = await docRf.get().toPromise();
      if(doc.exists) {
        const data = doc.data();
        delete data.companyId;
        delete data.createdByUid;
        delete data.createdDate;
        delete data.modifiedByUid;
        delete data.modifiedDate;
        return {status: 'ok', message: 'Success', payload: data}
      }     
    } catch(e) {
      console.log('Error', e)      
      this.presentToast(e.message, 'danger');
    }    
  }

  async getFlowerTypesList() {
    try {      
      const profile = await this.userService.getUser();             
      const list = await this.typeCollRf.ref.where('companyId', '==', profile.payload.companyId).get();      
      if(list.empty) {
        this.presentToast('No Flowers Types Found', 'danger');
        throw {status:'error', message: 'No Flower Types Found', payload: null};
      }      
      let arr = [];
      list.forEach(doc => {        
        const typeDoc = Object.assign({id: doc.id}, doc.data()); 
        arr.push(typeDoc);
      })    
      return {status: 'ok', message: 'Success', payload: arr}
    } catch(e) {
      this.presentToast(e.message, 'danger');
      console.log('Err', e);
    }
  }
  
  async deleteFlowerType(id: string) {
    try{
      const docRf = await this.typeCollRf.doc(id);
      const data = await docRf.delete();
      return DELETE_SUCCESS;
    } catch(e) {
      console.log('Delete Error', e);
      return {status: 'error', message: 'Delete Failed', payload: null};   
    }    
  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }
  
}
