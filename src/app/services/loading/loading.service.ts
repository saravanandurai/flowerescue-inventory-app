import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { LoadedRouterConfig } from '@angular/router/src/config';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loader  =  null;

  constructor(
    private loadingController: LoadingController,
  ) { 
  }

  async presentModal() {
    this.loader = await this.loadingController.create({
      spinner: 'crescent',
      cssClass: 'primary',
      message: 'Please wait!'
    });
    return await this.loader.present();
  }

  async dismissModal() {
    return await this.loader.dismiss();
  }

  


}
