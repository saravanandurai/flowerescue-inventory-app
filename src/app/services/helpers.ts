export const idGenerator = (prefix: string) => {    
    let ID_LEN = 10;
    let rand1 = Math.random().toString(36).slice(2);
    let rand2 = Math.random().toString(36).slice(2);
    let rand = rand1 + rand2;
    //dont change the seperator '-' . Especially not backslash as it will be a path
    let id = prefix + '-' + rand.substr(0,ID_LEN);
    return id;
}