import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore'

import Register from '../../interfaces/register';
import Login from '../../interfaces/login'; 
import { first } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private afAuth: AngularFireAuth,    
    private toastController: ToastController,
    private afStore: AngularFirestore,
  ) { }

  async register(registerForm: Register) {
    try{
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(registerForm.email, registerForm.password);
      console.log('REGIS RESULT', result);
      return {status: 'ok', message: 'Registered Successfully', payload: null}
    } catch(err) {
      console.log('Error', err)
      this.presentToast(err.message, 'danger');       
    }
  }

  async isLoggedIn() {
    const auth = await this.afAuth.authState.pipe(first()).toPromise();
    return auth? true : false; 
  }


  async login(loginForm: Login) {
    try{
      const result = await this.afAuth.auth.signInWithEmailAndPassword(loginForm.email, loginForm.password);
      console.log('LOGIN ', result);
      return {status: 'ok', message: 'Login Successful', payload: null}
    } catch(err) {
      console.log('Error: ', err);      
      this.presentToast(err.message, 'danger');
      throw {status: 'error', message: err.message, payload: null}
    }
  }

  async logout() {
    try {
      const result = await this.afAuth.auth.signOut();
      console.log('Logout Successful', result);                   
      return {status: 'ok', message: 'Logout Successful', payload: null}
    } catch (err) {
      console.log('Logout Error', err);
      this.presentToast(err.message, 'danger');
      throw {status: 'error', message: err.message, payload: null}
    }
  }
  
  
  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }


  
}
