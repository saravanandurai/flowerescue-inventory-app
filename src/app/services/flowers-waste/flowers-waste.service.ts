import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';

import { STORAGE_KEYS, SAVE_SUCCESS, DELETE_SUCCESS, UPDATE_SUCCESS, ACTION_FAILED } from '../../constants';
import FlowersWaste from '../../interfaces/flowersWaste';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class FlowersWasteService {
  fWCollRef: AngularFirestoreCollection; 
  constructor(    
    private db: AngularFirestore, 
    private toastController: ToastController,
    private afAuth: AngularFireAuth,
    private userService: UserService
  ) { 
    this.fWCollRef = this.db.collection(STORAGE_KEYS.FLOWERS_WASTE);
  }

  async getFlowersWaste(id: string) {
    try {
      const docRf = await this.fWCollRef.doc(id);
      const doc = await docRf.get().pipe(first()).toPromise();
      if(doc.exists) {
        const data = doc.data();
        delete data.companyId;
        delete data.createdByUid;
        delete data.createdDate;
        delete data.modifiedByUid;
        delete data.modifiedDate;
        
        return {status: 'ok', message: 'Success', payload: data};
      }      
      throw {status: 'error', message: 'Document does not exists', payload: null}
      
    } catch(e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }   
  }

  async getFlowersWasteList() {
    try {      
      const profile = await this.userService.getUser();
      const list = await this.fWCollRef.ref.where('companyId', '==', profile.payload.companyId).get();        
      if(list.empty) {
        throw {status: 'error', message: 'No Flowers Waste Found', payload: null}
      }
      let arr = [];
      list.forEach(doc => {      
        arr.push(Object.assign({id: doc.id}, doc.data()));
      })
      return {status: 'ok', message: 'Success', payload: arr};

    } catch (e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null};
    }    
  }

  async createFlowersWaste(flowersWaste: FlowersWaste) {    

    try {
      const authData = await this.afAuth.user.pipe(first()).toPromise();
      flowersWaste.createdByUid = authData.uid;
      flowersWaste.createdDate = new Date().toISOString();
      const profile = await this.userService.getUser();
      flowersWaste.companyId = profile.payload.companyId;   
      const result = await this.fWCollRef.add(flowersWaste);
      return {status: 'ok', message: 'Success', payload: result.id}
    } catch (e) {
      console.log('Error', e); 
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }    
  }

  
  async updateFlowersWaste(id: string, flowersWaste: FlowersWaste) {
    if(id === '0') {
      this.presentToast('Id cannot be 0 in update', 'danger');
      throw {status: 'error', message: 'Id cannot be 0 in update', payload: null};
    }

    try {
      const docRf = await this.fWCollRef.doc(id);
      const user = await this.afAuth.user.pipe(first()).toPromise();
      flowersWaste.modifiedByUid = user.uid;
      flowersWaste.modifiedDate = new Date().toISOString();
      const result = await docRf.update(flowersWaste);
      return UPDATE_SUCCESS;
    } catch(e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}
    }   
  }


  async deleteFlowersWaste(id: string) {
    if(id === '0') {
      this.presentToast('Id cannot be 0 in delete', 'danger');
      throw {status: 'error', message: 'Id cannot be 0 in delete', payload: null};
    }

    try {
      const docRf = await this.fWCollRef.doc(id);
      const status = await docRf.delete(); 
      return {status: 'ok', message: 'Deleted Successfully', payload: null};
    } catch(e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null}    
    }    
  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }

}
