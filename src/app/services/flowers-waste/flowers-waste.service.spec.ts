import { TestBed } from '@angular/core/testing';

import { FlowersWasteService } from './flowers-waste.service';

describe('FlowersWasteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlowersWasteService = TestBed.get(FlowersWasteService);
    expect(service).toBeTruthy();
  });
});
