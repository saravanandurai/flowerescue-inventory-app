import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'; 
import { STORAGE_KEYS, SAVE_SUCCESS, DELETE_SUCCESS, UPDATE_SUCCESS, ACTION_FAILED } from '../../constants'; 

import User from '../../interfaces/user';
import { ToastController } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userCollRef: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore,
    private toastController: ToastController,
    private afAuth:  AngularFireAuth,
  ) { 
    this.userCollRef = this.db.collection(STORAGE_KEYS.USER)
  }

  async createUser(user: User) {
        
    try {
      const userAuthData = await this.afAuth.authState.pipe(first()).toPromise();            
      const result = await this.userCollRef.doc(userAuthData.uid).set(user);
      return {status: 'ok', message: 'User Created', payload: null}
    } catch(e) {
      this.presentToast(e.message, 'danger');
    }
    
  }

  async getUser() {
    try {
      const user = await this.afAuth.user.pipe(first()).toPromise();  
      if(!user) {
        throw {status: 'error', message: 'Not enough permission. Login and try again. If problem persists contact Flowerescue', payload: null};
      }    
      const result = await this.userCollRef.doc(user.uid).get().pipe(first()).toPromise();      
      if(!result.exists) {
        throw {status: 'error', message: 'Document does not exists', payload: null};  
      }      
      return {status: 'ok', message: 'success', payload: result.data()};
      
    } catch (e) {
      this.presentToast(e.message, 'danger');
      throw {status: 'error', message: e.message, payload: null};
    }
  }



  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }
}
