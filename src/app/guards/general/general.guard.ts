import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service'

@Injectable({
  providedIn: 'root'
})
export class GeneralGuard implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router
  ) {

  }
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {

    const authState = await this.auth.isLoggedIn();
    if(authState) {
      return authState;
    } else {
      this.router.navigate(['login']);
      return false;
    }   
    
  }
}
