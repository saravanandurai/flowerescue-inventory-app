export default interface User {
    email: string,
    firstName: string, 
    lastName: string,
    company: string, 
    mobNo: string,
    companyId?: string,    
}