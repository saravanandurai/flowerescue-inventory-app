export default interface FlowerType {
    id: string,
    typeName: string,
    createdByUid?: string,
    createdDate?: string, 
    modifiedByUid?: string,
    modifiedDate?: string,
    companyId?: string,
} 