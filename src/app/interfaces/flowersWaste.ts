export default interface FlowersWaste {
    id?: string,
    date: string, 
    wasteArray: Array<{typeId: string, count: number}>,
    createdByUid?: string,
    createdDate?: string, 
    modifiedByUid?: string,
    modifiedDate?: string,
    companyId?: string,
}