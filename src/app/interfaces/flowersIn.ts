export default interface FlowersIn {
    id?: string,
    date: string, 
    inArray: Array<{typeId: string, count: number}>,
    createdByUid?: string,
    createdDate?: string, 
    modifiedByUid?: string,
    modifiedDate?: string,
    companyId?: string,
}