import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './services/auth/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss'], 
})
export class AppComponent {
  public appPages = [
    
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Flower Types List',
      url: '/flower-types-list',
      icon: 'list'
    },
    {
      title: 'Create Flower Type ',
      url: '/flower-type-master/0',
      icon: 'keypad'
    },
    {
      title: 'Flowers In',
      url: '/flowers-in/0',
      icon: 'add'
    },
    {
      title: 'Flowers In Multi',
      url: '/flowers-in-multi/0',
      icon: 'add'
    },
    {
      title: 'Flowers In List',
      url: '/flowers-in-list',
      icon: 'list'
    },
    {
      title: 'Flowers Waste',
      url: '/flowers-waste/0',
      icon: 'trash'
    },
    {
      title: 'Flowers Waste Multi',
      url: '/flowers-waste-multi/0',
      icon: 'add'
    },
    {
      title: 'Flowers Waste List',
      url: '/flowers-waste-list',
      icon: 'list'
    },
    {
      title: 'Flower Report',
      url: '/flower-report',
      icon: 'analytics'
    },       
  ];
  authState: boolean = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private auth: AuthService,
    private toastController: ToastController,
    private router: Router,
    
  ) {
    this.initializeApp();   
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.afAuth.authState.subscribe(val => {
        if(val) {
          this.authState = true;
        } else {
          this.authState = false;
        }
      })
    });
    
    
  }

  async logout() {
    try{
      const result = await this.auth.logout();
      this.router.navigate(['/login']);
      //Location reload to reload angular app and prevent old data from appearing in lazy loaded data.
      location.reload();
      this.presentToast(result.message, 'success');    
    } catch(e) {
      console.log('Logout Error', e)
    }
  }

  

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }
}
