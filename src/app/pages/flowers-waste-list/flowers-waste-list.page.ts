import { Component, OnInit } from '@angular/core';
import { FlowersWasteService } from '../../services/flowers-waste/flowers-waste.service';
import { FlowerTypesService } from 'src/app/services/flower-types/flower-types.service';
import { ToastController } from '@ionic/angular';
import FlowersWaste from '../../interfaces/flowersWaste'; 
import FlowerType from '../../interfaces/flowertype';
import { Router } from '@angular/router';
import { LoadingService } from '../../services/loading/loading.service';

@Component({
  selector: 'app-flowers-waste-list',
  templateUrl: './flowers-waste-list.page.html',
  styleUrls: ['./flowers-waste-list.page.scss'],
})
export class FlowersWasteListPage implements OnInit {
  flowersWasteList: Array<FlowersWaste> = [];
  flowerTypesList: Array<FlowerType> = [];  
  constructor(
    private fWService: FlowersWasteService,
    private fTService: FlowerTypesService,
    private toastController: ToastController,
    private router: Router,
    private loadingService: LoadingService,
  ) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    try {
      await this.loadingService.presentModal();
      const result = await this.fWService.getFlowersWasteList();
      this.flowersWasteList = result.payload;
      const flowerTypesResult = await this.fTService.getFlowerTypesList();
      this.flowerTypesList = flowerTypesResult.payload;
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error', e);
      await this.loadingService.dismissModal();
    }     
  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }

  goTo(id: string) {
    this.router.navigate(['/flowers-waste', id]);
  }

  typeName(typeId: string) {
    const item = this.flowerTypesList.find(val => val.id === typeId);
    return item ? item.typeName : '';
  }

}
