import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowersWasteListPage } from './flowers-waste-list.page';

describe('FlowersWasteListPage', () => {
  let component: FlowersWasteListPage;
  let fixture: ComponentFixture<FlowersWasteListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowersWasteListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowersWasteListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
