import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FlowersWasteListPage } from './flowers-waste-list.page';

const routes: Routes = [
  {
    path: '',
    component: FlowersWasteListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FlowersWasteListPage]
})
export class FlowersWasteListPageModule {}
