import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowerTypeMasterPage } from './flower-type-master.page';

describe('FlowerTypeMasterPage', () => {
  let component: FlowerTypeMasterPage;
  let fixture: ComponentFixture<FlowerTypeMasterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowerTypeMasterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowerTypeMasterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
