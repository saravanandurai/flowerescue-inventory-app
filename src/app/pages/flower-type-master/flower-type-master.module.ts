import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FlowerTypeMasterPage } from './flower-type-master.page';

const routes: Routes = [
  {
    path: '',
    component: FlowerTypeMasterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FlowerTypeMasterPage]
})
export class FlowerTypeMasterPageModule {}
