import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms'

import { ToastController } from '@ionic/angular'

import { FlowerTypesService } from '../../services/flower-types/flower-types.service'
import FlowerType from '../../interfaces/flowertype';
import { first } from 'rxjs/operators';
import { LoadingService }  from '../../services/loading/loading.service';


@Component({
  selector: 'app-flower-type-master',
  templateUrl: './flower-type-master.page.html',
  styleUrls: ['./flower-type-master.page.scss'],
})
export class FlowerTypeMasterPage implements OnInit {
  disabledFlag: boolean = false;
  title: string = 'Create Flower Type';
  //flowerType: FlowerType = FLOWERTYPE_INITIAL;
  submitted: boolean = false;
  flowerTypeForm: FormGroup;
  id: string = '0';

  constructor(
    private location: Location, 
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private ftService: FlowerTypesService,
    private toastController: ToastController, 
    private loadingService :LoadingService,   
  ) {
    this.flowerTypeForm = this.fb.group({      
      typeName: ['', [Validators.required, Validators.minLength(5)]]
    })
   }

  ngOnInit() {        
     
  }

  async ionViewWillEnter() {
    try {
      await this.loadingService.presentModal();
      const param = await this.activatedRoute.params.pipe(first()).toPromise();
      if(param.id !== '0') {
        const resultFT = await this.ftService.getFlowerType(param.id);
        this.flowerTypeForm.setValue(resultFT.payload);
        this.id=param.id;
        this.title = 'Flower Type';
        this.disabledFlag = true;
        this.flowerTypeForm.controls['typeName'].disable();
      } else {
        this.flowerTypeForm.setValue({
          typeName: ''          
        })
      }
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error in Type Master', e);
      await this.loadingService.dismissModal();
    }
    
    //TODO: Pop previous create page on just the create routine.
  }

  get f () {
    return this.flowerTypeForm.controls
  }

  cancel() {
    if(this.id === '0'){
      this.location.back();
    } else {
      this.disabledFlag = true;
      this.flowerTypeForm.disable(); 
      this.title =  'Flower Type'
    }
    
  }
  
  async saveFlowerType() {
    this.submitted = true;    
    if(this.flowerTypeForm.invalid) {
      return;
    }

    try {
      await this.loadingService.presentModal();
      if(this.id === '0') {      
        const result = await this.ftService.createFlowerType(this.flowerTypeForm.value);           
        this.router.navigate(['/flower-type-master', result.payload]);
        this.presentToast('Created Successfully', 'success');         
      } else {        
        const result = await this.ftService.updateFlowerType({id: this.id, typeName: this.flowerTypeForm.value.typeName});        
        this.disabledFlag = true; 
        this.flowerTypeForm.controls['typeName'].disable(); 
        this.title = 'Flower Type';
        this.presentToast(result.message, 'success');
      }
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error in Type Master', e);
      await this.loadingService.dismissModal();
    }           
  }

  editFlowerType() {    
    this.title = 'Edit Flower Type'; 
    this.flowerTypeForm.controls['typeName'].enable();
    this.disabledFlag = false;
  }

  
  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }

  viewEditCycle() {
    return this.id === '0' ? false : true; 
  }

  async delete() {
    try {
      await this.loadingService.presentModal();
      const result = await this.ftService.deleteFlowerType(this.id);
      this.router.navigate(['/flower-types-list']);
      this.presentToast(result.message, 'success');
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error in Flower Type Delete', e);
      this.loadingService.dismissModal();
    }  
  }
}
