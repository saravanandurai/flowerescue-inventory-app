import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController, IonItemSliding }  from '@ionic/angular';

import { FlowerTypesService } from '../../services/flower-types/flower-types.service'
import { FlowersInService } from '../../services/flowers-in/flowers-in.service';
import FlowerType from '../../interfaces/flowertype';
import { first } from 'rxjs/operators'
import { LoadingService } from '../../services/loading/loading.service';

@Component({
  selector: 'app-flowers-in',
  templateUrl: './flowers-in.page.html',
  styleUrls: ['./flowers-in.page.scss'],
})
export class FlowersInPage implements OnInit {
  maxDt: string = moment().format('YYYY-MM-DD');
  flowersInForm: FormGroup;
  disabledFlag: boolean = false;  
  flowerTypes: Array<FlowerType>;
  submitted: boolean = false;
  id: string = '0'; 
  typeActionSheet: any = {
    header: 'Flower Type',
    subHeader: 'Select a flower type'
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private fTService: FlowerTypesService,
    private toastController: ToastController,
    private fIService: FlowersInService,    
    private loadingService: LoadingService,
  ) {}

  ngOnInit() {
    this.flowersInForm = this.fb.group({
      date: [moment().toISOString(), Validators.required],
      inArray: this.fb.array([])
          
    })
    this.addFlowerInItem();
  }

  get inArrayForm() {
    return this.flowersInForm.get('inArray') as FormArray;
  }

  addFlowerInItem() {
    const flowerIn = this.fb.group({
      typeId: ['', Validators.required],
      count: [0, Validators.required]
    })

    this.inArrayForm.push(flowerIn);
  }

  deleteFlowerInItem(slidingItem: IonItemSliding , i: number) {
    slidingItem.close();
    this.inArrayForm.removeAt(i);
  }


  async ionViewWillEnter() {
    try {
      await this.loadingService.presentModal();
      //Getting FlowerTypes list
      const data = await this.fTService.getFlowerTypesList();     
      this.flowerTypes = data.payload;

      
      //Getting FlowersIn Tx.     
      const param = await this.activatedRoute.params.pipe(first()).toPromise();
      if(param.id !== '0') {
        const fIResult = await this.fIService.getFlowersIn(param.id); 
        const fIData = fIResult.payload; 
        if(fIData.inArray.length > 1) {
          for (let i = 1; i <= fIData.inArray.length - 1; i++){
            this.addFlowerInItem()            
          }             
        }
        this.flowersInForm.setValue(fIData);
        this.id = param.id;          
        this.disabledFlag = true;
        this.flowersInForm.disable(); 
      } else {
        //Resetting the values to intial on Flowers In Create
        this.flowersInForm.setValue({
          date: moment().toISOString(),
          inArray: [
            {
              typeId: '',
              count: 0
            }
          ]
        })
      }
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error Flowers In', e);
      await this.loadingService.dismissModal();
    }    
  }

  async saveFlowerIn() {
    console.log('Save', this.flowersInForm.value)
    this.submitted = true; 
    if(this.flowersInForm.invalid) {
      this.presentToast('All fields are mandatory!', 'danger');
      return;
    }

    try{
      await this.loadingService.presentModal();
      if(this.id === '0') {
        const result = await this.fIService.createFlowersIn(this.flowersInForm.value);     
        this.router.navigate(['/flowers-in', result.payload]);
        this.presentToast('Flowers In Created Successfully ', 'success')
        
      } else {
        const result = await this.fIService.updateFlowersIn(this.id, this.flowersInForm.value);        
        this.presentToast(result.message, 'success'); 
        this.disabledFlag = true; 
        this.flowersInForm.disable();        
      }
      await this.loadingService.dismissModal();    
    } catch(e) {
      console.log('Error in Flowers In Save', e);
      await this.loadingService.dismissModal();
    } 
  }
       
  

  async deleteFlowerIn() {
    try {
      await this.loadingService.presentModal();
      const result = await this.fIService.deleteFlowersIn(this.id); 
      await this.loadingService.dismissModal();
      this.router.navigate(['/flowers-in-list'])   
      this.presentToast(result.message, 'success');
    } catch (e) {
      console.log('Error in Flowers In Delete', e);
      await this.loadingService.dismissModal();
    }   
  }

  editFlowerIn() {
    this.disabledFlag = false;
    this.flowersInForm.enable();
  }


  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'middle',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,      
    });
    toast.present();
  }
}
