import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import FlowerType from '../../interfaces/flowertype';
import { FlowerTypesService } from '../../services/flower-types/flower-types.service';

import { ToastController } from '@ionic/angular'
import { database } from 'firebase';
import { LoadingService } from '../../services/loading/loading.service';

@Component({
  selector: 'app-flower-types-list',
  templateUrl: './flower-types-list.page.html',
  styleUrls: ['./flower-types-list.page.scss'],
})
export class FlowerTypesListPage implements OnInit {
  flowerTypesList: Array<FlowerType>; 

  constructor(
    private flowerTypesService: FlowerTypesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastController: ToastController,
    private loadingService: LoadingService,
  ) {}
  
  ngOnInit() {     
  }

  async ionViewWillEnter() {
    try{
      await this.loadingService.presentModal();
      const result = await this.flowerTypesService.getFlowerTypesList();      
      this.flowerTypesList = result.payload;
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error', e); 
    }    
  }

  goTo(id: string) {    
    this.router.navigate(['/flower-type-master', id])
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true       
    });
    toast.present();
  }

}
