import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowerTypesListPage } from './flower-types-list.page';

describe('FlowerTypesListPage', () => {
  let component: FlowerTypesListPage;
  let fixture: ComponentFixture<FlowerTypesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowerTypesListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowerTypesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
