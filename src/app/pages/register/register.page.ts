import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms'; 
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { UserService } from '../../services/user/user.service';
import { ToastController } from '@ionic/angular';
import User from '../../interfaces/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup; 
  submitted: boolean = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,  
    private toastController: ToastController,
    private userService: UserService,
  ) { }

  ngOnInit() {
    
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required], 
      company: ['', Validators.required],
      mobNo: ['', Validators.required],
      

    })
    
  }

  async register() {
    this.submitted = true; 

    if(this.registerForm.invalid) {
      this.presentToast('All Fields are mandatory or Error in form.', 'danger');
      return;
    }    

    if(this.registerForm.get('password').value !== this.registerForm.get('repeatPassword').value){
      this.presentToast('Password and Repeat Password donot match', 'danger');
      return;
    }

    try{
      const result = await this.authService.register(this.registerForm.value);  
      this.presentToast(result.message, 'success');  
    } catch (e) {
      console.log('in Reg err', e);
      return;
    } 

    try {                        
      const user: User = (({email, company, firstName, lastName, mobNo}) => ({email, company, firstName, lastName, mobNo}))(this.registerForm.value);
      const userResult = await this.userService.createUser(user)
      this.router.navigate(['/home'])       
    } catch(e) {
      console.log('in User err', e);      
    }     
  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }

}
