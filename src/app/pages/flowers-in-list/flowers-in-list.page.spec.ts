import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowersInListPage } from './flowers-in-list.page';

describe('FlowersInListPage', () => {
  let component: FlowersInListPage;
  let fixture: ComponentFixture<FlowersInListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowersInListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowersInListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
