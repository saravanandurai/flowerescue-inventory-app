import { Component, OnInit } from '@angular/core';
import { FlowersInService } from '../../services/flowers-in/flowers-in.service';
import { FlowerTypesService } from 'src/app/services/flower-types/flower-types.service';
import { ToastController } from '@ionic/angular';
import FlowersIn from '../../interfaces/flowersIn'; 
import FlowerType from '../../interfaces/flowertype';
import { Router } from '@angular/router';
import { LoadingService } from '../../services/loading/loading.service';

@Component({
  selector: 'app-flowers-in-list',
  templateUrl: './flowers-in-list.page.html',
  styleUrls: ['./flowers-in-list.page.scss'],
})
export class FlowersInListPage implements OnInit {
  flowersInList: Array<FlowersIn> = []; 
  flowerTypesList: Array<FlowerType> = [];  
  constructor(
    private fIService: FlowersInService,
    private fTService: FlowerTypesService,
    private toastController: ToastController,
    private router: Router,
    private loadingService: LoadingService,

  ) { }

  ngOnInit() {  
  }

  async ionViewWillEnter() {
    try{
      await this.loadingService.presentModal();
      const data = await this.fIService.getFlowersInList();
      if(data.status === 'error') {
        this.presentToast(data.message, 'danger');
        return;
      }
      this.flowersInList = data.payload;

      const fTData = await this.fTService.getFlowerTypesList();
      if(fTData.status === 'error') {
        this.presentToast(fTData.message, 'danger');
        return;
      }
      this.flowerTypesList = fTData.payload;
      this.loadingService.dismissModal();
    } catch(e) {
      console.log('Error', e);
      await this.loadingService.dismissModal();
    }    
  }

    
  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }

  goTo(id: string) {
    this.router.navigate(['/flowers-in', id]);
  }

  typeName(typeId: string) {
    const item = this.flowerTypesList.find(val => val.id === typeId);
    return item ? item.typeName : '';
  }

}
