import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowersWastePage } from './flowers-waste.page';

describe('FlowersWastePage', () => {
  let component: FlowersWastePage;
  let fixture: ComponentFixture<FlowersWastePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowersWastePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowersWastePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
