import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController, IonItemSliding }  from '@ionic/angular';

import { FlowerTypesService } from '../../services/flower-types/flower-types.service';
import { FlowersWasteService } from '../../services/flowers-waste/flowers-waste.service';
import FlowerType from '../../interfaces/flowertype';
import { first } from 'rxjs/operators'
import { LoadingService } from '../../services/loading/loading.service';

@Component({
  selector: 'app-flowers-waste',
  templateUrl: './flowers-waste.page.html',
  styleUrls: ['./flowers-waste.page.scss'],
})
export class FlowersWastePage implements OnInit {

  maxDt: string = moment().format('YYYY-MM-DD');
  flowersWasteForm: FormGroup;
  disabledFlag: boolean = false;
  flowerTypes: Array<FlowerType>; 
  submitted: boolean = false;
  id: string = '0';
  typeActionSheet: any = {
    header: 'Flower Type',
    subHeader: 'Select a flower type'
  }
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private fTService: FlowerTypesService,
    private toastController: ToastController,
    private fWService:  FlowersWasteService,
    private loadingService: LoadingService,
  ) { }

  ngOnInit() {
    this.flowersWasteForm = this.fb.group({
      date: [moment().toISOString(), Validators.required],
      wasteArray: this.fb.array([])          
    })
    this.addFlowerWasteItem();
  }

  get wasteArrayForm() {
    return this.flowersWasteForm.get('wasteArray') as FormArray;
  }

  addFlowerWasteItem() {
    const flowerWaste = this.fb.group({
      typeId: ['', Validators.required],
      count: [0, Validators.required]
    })

    this.wasteArrayForm.push(flowerWaste);
  }

  deleteFlowerWasteItem(slidingItem: IonItemSliding , i: number) {
    slidingItem.close();
    this.wasteArrayForm.removeAt(i);
  }

  
  async ionViewWillEnter() {
    try {
      await this.loadingService.presentModal();
      //Getting FlowerTypes list
      const result = await this.fTService.getFlowerTypesList();       
      this.flowerTypes = result.payload;      
      //Getting FlowersWaste Tx.
      const param = await this.activatedRoute.params.pipe(first()).toPromise();
      if(param.id !== '0') {
        const resultFW = await this.fWService.getFlowersWaste(param.id);
        const fWData = resultFW.payload;
        if(fWData.wasteArray.length > 1) {
          for (let i = 1; i <= fWData.wasteArray.length - 1; i++){
            this.addFlowerWasteItem()            
          }             
        }       
        this.flowersWasteForm.setValue(fWData);
        this.id = param.id;          
        this.disabledFlag = true;
        this.flowersWasteForm.disable();  
      } else {
        //Resetting the values to intial on Flowers Waste Create
        this.flowersWasteForm.setValue({
          date: moment().toISOString(),
          wasteArray: [
            {
              typeId: '',
              count: 0
            }
          ]
        })
      }
      await this.loadingService.dismissModal();
    } catch(e) {
      console.log('Error', e);
      await this.loadingService.dismissModal();
    }    
  }

  
  async saveFlowerWaste() {
    console.log('Save', this.flowersWasteForm.value)
    this.submitted = true; 
    if(this.flowersWasteForm.invalid) {
      this.presentToast('All fields are mandatory!', 'danger');
      return;
    }

    try {
      await this.loadingService.presentModal();
      if(this.id === '0') { 
        const result = await this.fWService.createFlowersWaste(this.flowersWasteForm.value);
        this.router.navigate(['/flowers-waste', result.payload])
        this.presentToast('FlowersWasteCreatedSuccessfully', 'success');
      } else {
        const result = await this.fWService.updateFlowersWaste(this.id, this.flowersWasteForm.value);
        this.presentToast(result.message, 'success'); 
        this.disabledFlag = true; 
        this.flowersWasteForm.disable(); 
      }
      await this.loadingService.dismissModal();
    } catch (e) {
      console.log('Error', e.message);
      await this.loadingService.dismissModal();
    }
  }

  
  async deleteFlowerWaste() {
    try {
      await this.loadingService.presentModal();
      const result = await this.fWService.deleteFlowersWaste(this.id);
      await this.loadingService.dismissModal();
      this.router.navigate(['/flowers-waste-list']); 
      this.presentToast(result.message, 'success');   
    } catch (e) {
      console.log('Error', e);
      await this.loadingService.dismissModal();
    }    
  }

  editFlowerWaste() {
    this.disabledFlag = false;
    this.flowersWasteForm.enable();

  }

  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }


}
