import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms'; 
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;

  constructor(
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }

  async login() {
    this.submitted = true;
    if(this.loginForm.invalid) {
      this.presentToast('All fields are required or Email invalid', 'danger');
      return;
    }

    try {
      const result = await this.auth.login(this.loginForm.value)
      this.router.navigate(['/home']);
      this.presentToast(result.message, 'success');
    } catch(e) {
      console.log('Login Error', e);
    }    
  }


  async presentToast(message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      closeButtonText: 'Done',
      showCloseButton: true,
      color: color,
    });
    toast.present();
  }


}
