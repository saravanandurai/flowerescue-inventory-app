import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-flowers-in-multi',
  templateUrl: './flowers-in-multi.page.html',
  styleUrls: ['./flowers-in-multi.page.scss'],
})
export class FlowersInMultiPage implements OnInit {

  finDate: string = moment().format('YYYY-MM-DD');
  flowersInMultiForm: FormGroup;
  flowersList = [1, 2, 3];
  
  suppliers = [
    {
      supplierId: '1',
      supplierName: 'Supplier 1'
    },
    {
      supplierId: '2',
      supplierName: 'Supplier 2'
    },
    {
      supplierId: '3',
      supplierName: 'Supplier 3'
    },
    {
      supplierId: '4',
      supplierName: 'Supplier 4'
    },
  ]

  flowerTypes = [
    {
      id: '1',
      typeName: 'Flower Type 1'
    },
    {
      id: '2',
      typeName: 'Flower Type 2'
    },
    {
      id: '3',
      typeName: 'Flower Type 3'
    },
    {
      id: '4',
      typeName: 'Flower Type 4'
    },
  ]
  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
  }

}
