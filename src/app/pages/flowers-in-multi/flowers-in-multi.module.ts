import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FlowersInMultiPage } from './flowers-in-multi.page';

const routes: Routes = [
  {
    path: '',
    component: FlowersInMultiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FlowersInMultiPage]
})
export class FlowersInMultiPageModule {}
