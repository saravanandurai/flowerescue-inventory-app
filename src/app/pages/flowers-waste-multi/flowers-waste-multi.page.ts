import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flowers-waste-multi',
  templateUrl: './flowers-waste-multi.page.html',
  styleUrls: ['./flowers-waste-multi.page.scss'],
})
export class FlowersWasteMultiPage implements OnInit {

  sample = [
    {
      txNo: 23,
      date: '2019-02-23',
      supplier: 'Supplier 1',
      flowers:[
        {
          type: 'Type 1',
          count: 234,
        },
        {
          type: 'Type 2',
          count: 104,
        },
        {
          type: 'Type 3',
          count: 45,
        },
        {
          type: 'Type 4',
          count: 500,
        },        
      ]
    },
    {
      txNo: 12,
      date: '2019-02-16',
      supplier: 'Supplier 2',
      flowers:[
        {
          type: 'Type 3',
          count: 170,
        },
        {
          type: 'Type 5',
          count: 10,
        },
        {
          type: 'Type 7',
          count: 25,
        },
        {
          type: 'Type 4',
          count: 300,
        },        
      ]
    }
  ]
  data1 = ['Type 1', 'Type 3', 'Type 2', 'Type 4', ]
  data2 = ['Type 1', 'Type 5', 'Type 8', 'Type 12', ]
  constructor() { }

  ngOnInit() {
  }

}
