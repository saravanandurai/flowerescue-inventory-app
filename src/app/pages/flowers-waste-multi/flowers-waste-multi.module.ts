import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FlowersWasteMultiPage } from './flowers-waste-multi.page';

const routes: Routes = [
  {
    path: '',
    component: FlowersWasteMultiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FlowersWasteMultiPage]
})
export class FlowersWasteMultiPageModule {}
