import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowerReportPage } from './flower-report.page';

describe('FlowerReportPage', () => {
  let component: FlowerReportPage;
  let fixture: ComponentFixture<FlowerReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowerReportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowerReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
