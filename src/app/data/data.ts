import FlowerType from '../interfaces/flowertype';

export const FLOWERTYPESLIST: Array<FlowerType> = [
    {
        id: '1', 
        typeName: 'Red Roses',        
    },
    {
        id: '2', 
        typeName: 'White Roses',        
    },
    {
        id: '3', 
        typeName: 'Yellow Tulips',        
    },
    {
        id: '4', 
        typeName: 'White Lillies',        
    },
    {
        id: '5', 
        typeName: 'Green Ferns',        
    },
    {
        id: '6', 
        typeName: 'Red Hibuscus',        
    }
]