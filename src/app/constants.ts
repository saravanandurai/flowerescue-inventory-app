import { GetOptions } from 'firebase/firestore';

export const STORAGE_KEYS = {
    FLOWER_TYPES : 'flowerTypes', 
    FLOWERS_IN: 'flowersIn', 
    FLOWERS_WASTE: 'flowersWaste',
    USER: 'user',
}

export const SAVE_SUCCESS = {
    status: 'ok', 
    message: 'Saved Successfully',
    payload: {}
}

export const UPDATE_SUCCESS = {
    status: 'ok', 
    message: 'Updated Successfully',
    payload: {}
}

export const DELETE_SUCCESS = {
    status: 'ok', 
    message: 'Deleted Successfully',
    payload: {}
}

export const ACTION_FAILED = {
    status: 'error', 
    message: 'Error. Contact Flowerescue',
    payload: {}
}

export const GET_OPTIONS: GetOptions  = {
    source: 'cache'
}